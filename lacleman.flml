<?xml version='1.0' encoding='utf-8'?>
<fluidity_options>
  <simulation_name>
    <string_value lines="1">lacleman</string_value>
  </simulation_name>
  <problem_type>
    <string_value lines="1">oceans</string_value>
  </problem_type>
  <geometry>
    <dimension>
      <integer_value rank="0">3</integer_value>
    </dimension>
    <mesh name="CoordinateMesh">
      <from_mesh>
        <mesh name="BaseMesh"/>
        <stat>
          <include_in_stat/>
        </stat>
      </from_mesh>
    </mesh>
    <mesh name="VelocityMesh">
      <from_mesh>
        <mesh name="CoordinateMesh"/>
        <stat>
          <exclude_from_stat/>
        </stat>
      </from_mesh>
    </mesh>
    <mesh name="2DMesh">
      <from_file file_name="lacleman">
        <format name="gmsh"/>
        <stat>
          <exclude_from_stat/>
        </stat>
      </from_file>
    </mesh>
    <mesh name="BaseMesh">
      <from_mesh>
        <mesh name="2DMesh"/>
        <extrude>
          <regions name="WholeMesh">
            <bottom_depth>
              <from_map file_name="Bathymetry.grd">
                <min_depth>
                  <real_value rank="0">1.5</real_value>
                </min_depth>
              </from_map>
            </bottom_depth>
            <sizing_function>
              <list>
                <real_value shape="20" rank="1">0.5 1.0 2.0 2.0 5.0 5.0 5.0 5.0 5.0 10.0 10.0 10.0 10.0 20.0 20.0 20.0 40.0 40.0 50.0 50.0</real_value>
              </list>
            </sizing_function>
            <minimum_bottom_layer_fraction>
              <real_value rank="0">0.05</real_value>
            </minimum_bottom_layer_fraction>
            <top_surface_id>
              <integer_value rank="0">2001</integer_value>
            </top_surface_id>
            <bottom_surface_id>
              <integer_value rank="0">2002</integer_value>
            </bottom_surface_id>
          </regions>
        </extrude>
        <stat>
          <exclude_from_stat/>
        </stat>
      </from_mesh>
    </mesh>
    <mesh name="GeostrophicPressureMesh">
      <from_mesh>
        <mesh name="CoordinateMesh"/>
        <mesh_shape>
          <polynomial_degree>
            <integer_value rank="0">2</integer_value>
          </polynomial_degree>
        </mesh_shape>
        <stat>
          <exclude_from_stat/>
        </stat>
      </from_mesh>
    </mesh>
    <quadrature>
      <degree>
        <integer_value rank="0">4</integer_value>
      </degree>
    </quadrature>
    <ocean_boundaries>
      <top_surface_ids>
        <integer_value shape="1" rank="1">2001</integer_value>
      </top_surface_ids>
      <bottom_surface_ids>
        <integer_value shape="1" rank="1">2002</integer_value>
      </bottom_surface_ids>
      <scalar_field name="DistanceToTop" rank="0">
        <diagnostic>
          <algorithm name="Internal" material_phase_support="multiple"/>
          <mesh name="CoordinateMesh"/>
          <output/>
          <stat/>
          <convergence>
            <include_in_convergence/>
          </convergence>
          <detectors>
            <include_in_detectors/>
          </detectors>
          <steady_state>
            <include_in_steady_state/>
          </steady_state>
        </diagnostic>
      </scalar_field>
      <scalar_field name="DistanceToBottom" rank="0">
        <diagnostic>
          <algorithm name="Internal" material_phase_support="multiple"/>
          <mesh name="CoordinateMesh"/>
          <output/>
          <stat/>
          <convergence>
            <include_in_convergence/>
          </convergence>
          <detectors>
            <include_in_detectors/>
          </detectors>
          <steady_state>
            <include_in_steady_state/>
          </steady_state>
        </diagnostic>
      </scalar_field>
    </ocean_boundaries>
  </geometry>
  <io>
    <dump_format>
      <string_value>vtk</string_value>
    </dump_format>
    <dump_period>
      <python>
        <string_value lines="20" type="code" language="python">def val(t):
  if t&lt;=86400:
    return 1800
  else:
    return 10800</string_value>
      </python>
    </dump_period>
    <output_mesh name="CoordinateMesh"/>
    <checkpointing>
      <checkpoint_period_in_dumps>
        <integer_value rank="0">80</integer_value>
      </checkpoint_period_in_dumps>
      <checkpoint_at_end/>
    </checkpointing>
    <stat/>
  </io>
  <timestepping>
    <current_time>
      <real_value rank="0">0.0</real_value>
      <time_units date="seconds since 2011-11-1 00:00:0.0"/>
    </current_time>
    <timestep>
      <real_value rank="0">10.</real_value>
    </timestep>
    <finish_time>
      <real_value rank="0">2592000</real_value>
      <comment>86400 * (30 days nov) = 2592000</comment>
    </finish_time>
    <nonlinear_iterations>
      <integer_value rank="0">2</integer_value>
    </nonlinear_iterations>
    <adaptive_timestep>
      <requested_cfl>
        <real_value rank="0">0.3</real_value>
      </requested_cfl>
      <courant_number name="CFLNumber">
        <mesh name="CoordinateMesh"/>
      </courant_number>
      <minimum_timestep>
        <terminate_if_reached/>
        <real_value rank="0">10.0</real_value>
      </minimum_timestep>
      <increase_tolerance>
        <real_value rank="0">1.1</real_value>
      </increase_tolerance>
    </adaptive_timestep>
  </timestepping>
  <physical_parameters>
    <gravity>
      <magnitude>
        <real_value rank="0">9.81</real_value>
      </magnitude>
      <vector_field name="GravityDirection" rank="1">
        <prescribed>
          <mesh name="CoordinateMesh"/>
          <value name="WholeMesh">
            <constant>
              <real_value shape="3" dim1="dim" rank="1">0 0 -1</real_value>
            </constant>
          </value>
          <output/>
          <stat>
            <include_in_stat/>
          </stat>
          <detectors>
            <exclude_from_detectors/>
          </detectors>
        </prescribed>
      </vector_field>
    </gravity>
    <coriolis>
      <f_plane>
        <f>
          <real_value rank="0">1.046e-4</real_value>
        </f>
      </f_plane>
    </coriolis>
  </physical_parameters>
  <material_phase name="Fields">
    <equation_of_state>
      <fluids>
        <linear>
          <reference_density>
            <real_value rank="0">1.0</real_value>
          </reference_density>
          <temperature_dependency>
            <reference_temperature>
              <real_value rank="0">0.0</real_value>
            </reference_temperature>
            <thermal_expansion_coefficient>
              <real_value rank="0">0.0001</real_value>
            </thermal_expansion_coefficient>
          </temperature_dependency>
          <subtract_out_hydrostatic_level/>
        </linear>
      </fluids>
    </equation_of_state>
    <subgridscale_parameterisations>
      <GLS>
        <option>
          <string_value>k-epsilon</string_value>
        </option>
        <stability_function>
          <string_value>Canuto-01-A</string_value>
        </stability_function>
        <calculate_boundaries>
          <string_value>neumann</string_value>
          <top_surface_ids>
            <integer_value shape="1" rank="1">2001</integer_value>
          </top_surface_ids>
          <bottom_surface_ids>
            <integer_value shape="1" rank="1">2002</integer_value>
          </bottom_surface_ids>
        </calculate_boundaries>
        <scalar_field name="GLSTurbulentKineticEnergy" rank="0">
          <prognostic>
            <mesh name="CoordinateMesh"/>
            <equation name="AdvectionDiffusion"/>
            <spatial_discretisation>
              <continuous_galerkin>
                <stabilisation>
                  <streamline_upwind>
                    <nu_bar_critical_rule/>
                    <nu_scale name="0.5">
                      <real_value shape="1" rank="0">0.5</real_value>
                    </nu_scale>
                  </streamline_upwind>
                </stabilisation>
                <advection_terms/>
                <mass_terms/>
              </continuous_galerkin>
              <conservative_advection>
                <real_value rank="0">0.0</real_value>
              </conservative_advection>
            </spatial_discretisation>
            <temporal_discretisation>
              <theta>
                <real_value rank="0">1.0</real_value>
              </theta>
            </temporal_discretisation>
            <solver>
              <iterative_method name="gmres">
                <restart>
                  <integer_value rank="0">30</integer_value>
                </restart>
              </iterative_method>
              <preconditioner name="sor"/>
              <relative_error>
                <real_value rank="0">1.0e-7</real_value>
              </relative_error>
              <max_iterations>
                <integer_value rank="0">1000</integer_value>
              </max_iterations>
              <never_ignore_solver_failures/>
              <diagnostics>
                <monitors/>
              </diagnostics>
            </solver>
            <initial_condition name="WholeMesh">
              <constant>
                <real_value rank="0">1.0e-6</real_value>
              </constant>
            </initial_condition>
            <tensor_field name="Diffusivity" rank="2">
              <diagnostic>
                <algorithm name="Internal" material_phase_support="multiple"/>
                <output>
                  <exclude_from_vtu/>
                </output>
                <stat>
                  <exclude_from_stat/>
                </stat>
              </diagnostic>
            </tensor_field>
            <scalar_field name="Source" rank="0">
              <diagnostic>
                <algorithm name="Internal" material_phase_support="multiple"/>
                <output>
                  <exclude_from_vtu/>
                </output>
                <stat>
                  <exclude_from_stat/>
                </stat>
                <detectors>
                  <exclude_from_detectors/>
                </detectors>
              </diagnostic>
            </scalar_field>
            <scalar_field name="Absorption" rank="0">
              <diagnostic>
                <algorithm name="Internal" material_phase_support="multiple"/>
                <output>
                  <exclude_from_vtu/>
                </output>
                <stat>
                  <exclude_from_stat/>
                </stat>
                <detectors>
                  <exclude_from_detectors/>
                </detectors>
              </diagnostic>
            </scalar_field>
            <output/>
            <stat/>
            <convergence>
              <include_in_convergence/>
            </convergence>
            <detectors>
              <include_in_detectors/>
            </detectors>
            <steady_state>
              <include_in_steady_state/>
            </steady_state>
            <consistent_interpolation/>
            <minimum_value>
              <real_value rank="0">5.0e-7</real_value>
            </minimum_value>
          </prognostic>
        </scalar_field>
        <scalar_field name="GLSGenericSecondQuantity" rank="0">
          <prognostic>
            <mesh name="CoordinateMesh"/>
            <equation name="AdvectionDiffusion"/>
            <spatial_discretisation>
              <continuous_galerkin>
                <stabilisation>
                  <streamline_upwind>
                    <nu_bar_critical_rule/>
                    <nu_scale name="0.5">
                      <real_value shape="1" rank="0">0.5</real_value>
                    </nu_scale>
                  </streamline_upwind>
                </stabilisation>
                <advection_terms/>
                <mass_terms/>
              </continuous_galerkin>
              <conservative_advection>
                <real_value rank="0">0.0</real_value>
              </conservative_advection>
            </spatial_discretisation>
            <temporal_discretisation>
              <theta>
                <real_value rank="0">1.0</real_value>
              </theta>
            </temporal_discretisation>
            <solver>
              <iterative_method name="gmres">
                <restart>
                  <integer_value rank="0">30</integer_value>
                </restart>
              </iterative_method>
              <preconditioner name="sor"/>
              <relative_error>
                <real_value rank="0">1.0e-7</real_value>
              </relative_error>
              <max_iterations>
                <integer_value rank="0">1000</integer_value>
              </max_iterations>
              <never_ignore_solver_failures/>
              <diagnostics>
                <monitors/>
              </diagnostics>
            </solver>
            <initial_condition name="WholeMesh">
              <constant>
                <real_value rank="0">1.0e-8</real_value>
              </constant>
            </initial_condition>
            <tensor_field name="Diffusivity" rank="2">
              <diagnostic>
                <algorithm name="Internal" material_phase_support="multiple"/>
                <output>
                  <exclude_from_vtu/>
                </output>
                <stat>
                  <exclude_from_stat/>
                </stat>
              </diagnostic>
            </tensor_field>
            <scalar_field name="Source" rank="0">
              <diagnostic>
                <algorithm name="Internal" material_phase_support="multiple"/>
                <output>
                  <exclude_from_vtu/>
                </output>
                <stat>
                  <exclude_from_stat/>
                </stat>
                <detectors>
                  <exclude_from_detectors/>
                </detectors>
              </diagnostic>
            </scalar_field>
            <scalar_field name="Absorption" rank="0">
              <diagnostic>
                <algorithm name="Internal" material_phase_support="multiple"/>
                <output>
                  <exclude_from_vtu/>
                </output>
                <stat>
                  <exclude_from_stat/>
                </stat>
                <detectors>
                  <exclude_from_detectors/>
                </detectors>
              </diagnostic>
            </scalar_field>
            <output/>
            <stat/>
            <convergence>
              <include_in_convergence/>
            </convergence>
            <detectors>
              <include_in_detectors/>
            </detectors>
            <steady_state>
              <include_in_steady_state/>
            </steady_state>
            <consistent_interpolation/>
          </prognostic>
        </scalar_field>
        <tensor_field name="GLSBackgroundViscosity" rank="2">
          <prescribed>
            <mesh name="CoordinateMesh"/>
            <value name="WholeMesh">
              <diagonal>
                <constant>
                  <real_value shape="3" dim1="dim" rank="1">0.01 0.01 1.0e-5</real_value>
                </constant>
              </diagonal>
            </value>
            <output>
              <exclude_from_vtu/>
            </output>
          </prescribed>
        </tensor_field>
        <tensor_field name="GLSBackgroundDiffusivity" rank="2">
          <prescribed>
            <mesh name="CoordinateMesh"/>
            <value name="WholeMesh">
              <diagonal>
                <constant>
                  <real_value shape="3" dim1="dim" rank="1">0.01 0.01 1.0e-5</real_value>
                </constant>
              </diagonal>
            </value>
            <output/>
          </prescribed>
        </tensor_field>
        <tensor_field name="GLSEddyViscosityKM" rank="2">
          <diagnostic>
            <algorithm name="Internal" material_phase_support="multiple"/>
            <mesh name="CoordinateMesh"/>
            <output/>
            <stat>
              <exclude_from_stat/>
            </stat>
            <convergence>
              <include_in_convergence/>
            </convergence>
            <detectors>
              <exclude_from_detectors/>
            </detectors>
            <steady_state>
              <include_in_steady_state/>
            </steady_state>
          </diagnostic>
        </tensor_field>
        <tensor_field name="GLSEddyDiffusivityKH" rank="2">
          <diagnostic>
            <algorithm name="Internal" material_phase_support="multiple"/>
            <mesh name="CoordinateMesh"/>
            <output/>
            <stat>
              <exclude_from_stat/>
            </stat>
            <convergence>
              <include_in_convergence/>
            </convergence>
            <detectors>
              <exclude_from_detectors/>
            </detectors>
            <steady_state>
              <include_in_steady_state/>
            </steady_state>
          </diagnostic>
        </tensor_field>
        <scalar_field name="GLSBuoyancyFrequency" rank="0">
          <diagnostic>
            <algorithm name="Internal" material_phase_support="multiple"/>
            <mesh name="CoordinateMesh"/>
            <output/>
            <stat>
              <exclude_from_stat/>
            </stat>
            <convergence>
              <include_in_convergence/>
            </convergence>
            <detectors>
              <exclude_from_detectors/>
            </detectors>
            <steady_state>
              <include_in_steady_state/>
            </steady_state>
          </diagnostic>
        </scalar_field>
        <scalar_field name="GLSVelocityShear" rank="0">
          <diagnostic>
            <algorithm name="Internal" material_phase_support="multiple"/>
            <mesh name="CoordinateMesh"/>
            <output/>
            <stat>
              <exclude_from_stat/>
            </stat>
            <convergence>
              <include_in_convergence/>
            </convergence>
            <detectors>
              <exclude_from_detectors/>
            </detectors>
            <steady_state>
              <include_in_steady_state/>
            </steady_state>
          </diagnostic>
        </scalar_field>
      </GLS>
    </subgridscale_parameterisations>
    <scalar_field name="Pressure" rank="0">
      <prognostic>
        <mesh name="CoordinateMesh"/>
        <spatial_discretisation>
          <continuous_galerkin>
            <remove_stabilisation_term/>
            <integrate_continuity_by_parts/>
          </continuous_galerkin>
        </spatial_discretisation>
        <scheme>
          <poisson_pressure_solution>
            <string_value lines="1">only first timestep</string_value>
          </poisson_pressure_solution>
          <use_projection_method/>
        </scheme>
        <solver>
          <iterative_method name="cg"/>
          <preconditioner name="mg">
            <vertical_lumping/>
          </preconditioner>
          <relative_error>
            <real_value rank="0">1.0e-7</real_value>
          </relative_error>
          <absolute_error>
            <real_value rank="0">0.0</real_value>
          </absolute_error>
          <max_iterations>
            <integer_value rank="0">2000</integer_value>
          </max_iterations>
          <never_ignore_solver_failures/>
          <diagnostics>
            <monitors/>
          </diagnostics>
        </solver>
        <initial_condition name="WholeMesh">
          <constant>
            <real_value rank="0">0.0</real_value>
          </constant>
        </initial_condition>
        <output/>
        <stat>
          <exclude_from_stat/>
        </stat>
        <convergence>
          <include_in_convergence/>
        </convergence>
        <detectors>
          <exclude_from_detectors/>
        </detectors>
        <steady_state>
          <include_in_steady_state/>
        </steady_state>
        <consistent_interpolation/>
      </prognostic>
    </scalar_field>
    <scalar_field name="Density" rank="0">
      <diagnostic>
        <algorithm name="Internal" material_phase_support="multiple"/>
        <mesh name="CoordinateMesh"/>
        <output/>
        <stat>
          <exclude_from_stat/>
        </stat>
        <convergence>
          <include_in_convergence/>
        </convergence>
        <detectors>
          <include_in_detectors/>
        </detectors>
        <steady_state>
          <include_in_steady_state/>
        </steady_state>
      </diagnostic>
    </scalar_field>
    <vector_field name="Velocity" rank="1">
      <prognostic>
        <mesh name="CoordinateMesh"/>
        <equation name="Boussinesq"/>
        <spatial_discretisation>
          <continuous_galerkin>
            <stabilisation>
              <streamline_upwind>
                <nu_bar_optimal/>
                <nu_scale name="0.5">
                  <real_value shape="1" rank="0">0.5</real_value>
                </nu_scale>
              </streamline_upwind>
            </stabilisation>
            <mass_terms>
              <lump_mass_matrix/>
            </mass_terms>
            <advection_terms>
              <integrate_advection_by_parts/>
            </advection_terms>
            <stress_terms>
              <tensor_form/>
            </stress_terms>
            <buoyancy/>
          </continuous_galerkin>
          <conservative_advection>
            <real_value rank="0">0.0</real_value>
          </conservative_advection>
        </spatial_discretisation>
        <temporal_discretisation>
          <theta>
            <real_value rank="0">1.0</real_value>
          </theta>
          <relaxation>
            <real_value rank="0">1.0</real_value>
          </relaxation>
        </temporal_discretisation>
        <solver>
          <iterative_method name="gmres">
            <restart>
              <integer_value rank="0">30</integer_value>
            </restart>
          </iterative_method>
          <preconditioner name="sor"/>
          <relative_error>
            <real_value rank="0">1.0e-7</real_value>
          </relative_error>
          <absolute_error>
            <real_value rank="0">0.0</real_value>
          </absolute_error>
          <max_iterations>
            <integer_value rank="0">10000</integer_value>
          </max_iterations>
          <never_ignore_solver_failures/>
          <diagnostics>
            <monitors/>
          </diagnostics>
        </solver>
        <initial_condition name="WholeMesh">
          <constant>
            <real_value shape="3" dim1="dim" rank="1">0.0 0.0 0.0</real_value>
          </constant>
        </initial_condition>
        <boundary_conditions name="drag_on_bottom">
          <surface_ids>
            <integer_value shape="1" rank="1">2002</integer_value>
          </surface_ids>
          <type name="drag">
            <constant>
              <real_value rank="0">0.0025</real_value>
            </constant>
            <quadratic_drag/>
          </type>
        </boundary_conditions>
        <boundary_conditions name="FreeSurface">
          <surface_ids>
            <integer_value shape="1" rank="1">2001</integer_value>
          </surface_ids>
          <type name="free_surface"/>
        </boundary_conditions>
        <boundary_conditions name="no_flow_sides">
          <surface_ids>
            <integer_value shape="4" rank="1">1001 1002 1003 1004</integer_value>
          </surface_ids>
          <type name="no_normal_flow"/>
        </boundary_conditions>
        <boundary_conditions name="no_flow_bottom">
          <surface_ids>
            <integer_value shape="1" rank="1">2002</integer_value>
          </surface_ids>
          <type name="no_normal_flow"/>
        </boundary_conditions>
        <boundary_conditions name="drag_sides">
          <surface_ids>
            <integer_value shape="4" rank="1">1001 1002 1003 1004</integer_value>
          </surface_ids>
          <type name="drag">
            <constant>
              <real_value rank="0">0.0025</real_value>
            </constant>
            <quadratic_drag/>
          </type>
        </boundary_conditions>
        <boundary_conditions name="wind_forcing">
          <surface_ids>
            <integer_value shape="1" rank="1">2001</integer_value>
          </surface_ids>
          <type name="wind_forcing">
            <wind_stress>
              <python>
                <string_value lines="20" type="code" language="python">def val(X,t):
  if t &lt; 3600:
    return (0.0, 0.0)
  from scipy.io.netcdf import netcdf_file as ncfile
  from scipy.interpolate import interp2d
  from numpy import where
  from math import sqrt, log10
  
  def stress(u, v, rhoA=1.3e-3):
    # C = 1.0e-3

    u2 = u*u
    v2 = v*v
    speed = sqrt(u2 + v2)
    if speed==0.0:
      return (0.0, 0.0)
    elif speed&lt;5:
        C = 0.0044*(speed**(-1.15))
    else:
        C = 0.001 * log10(speed)
    C *=  rhoA
    # it must be proportional to module^2,
    # but with the right sign
    return (C * u * sqrt(u2), C * v * sqrt(v2))

  f = ncfile("preprocess/COSMO2fluidity_wind_01-11-2011_31-12-2011.nc", 'r')
  
  time = f.variables["time"][:]
  ind_t = where(time&gt;t)[0][0] - 1
  
  # these are actually in CH1903 projections
  xu = f.variables["longitude"][:]
  yu = f.variables["latitude"][:]

  U10 = f.variables["U10"][ind_t, ...]
  V10 = f.variables["V10"][ind_t, ...]
  
  f.close()
  
  f = interp2d(xu, yu, U10)
  U10 = f(X[0], X[1])
  f = interp2d(xu, yu, V10)
  V10 = f(X[0], X[1])
                 
  return stress(U10, V10)</string_value>
              </python>
            </wind_stress>
          </type>
        </boundary_conditions>
        <tensor_field name="Viscosity" rank="2">
          <diagnostic>
            <algorithm name="Internal" material_phase_support="multiple"/>
            <output/>
            <stat>
              <include_in_stat/>
            </stat>
          </diagnostic>
        </tensor_field>
        <vertical_stabilization>
          <vertical_velocity_relaxation>
            <scale_factor>
              <real_value rank="0">1.0e-3</real_value>
            </scale_factor>
          </vertical_velocity_relaxation>
          <implicit_buoyancy/>
        </vertical_stabilization>
        <output/>
        <stat>
          <include_in_stat/>
          <previous_time_step>
            <exclude_from_stat/>
          </previous_time_step>
          <nonlinear_field>
            <exclude_from_stat/>
          </nonlinear_field>
        </stat>
        <convergence>
          <include_in_convergence/>
        </convergence>
        <detectors>
          <include_in_detectors/>
        </detectors>
        <steady_state>
          <include_in_steady_state/>
        </steady_state>
        <consistent_interpolation/>
      </prognostic>
    </vector_field>
    <scalar_field name="FreeSurface" rank="0">
      <diagnostic>
        <algorithm name="Internal" material_phase_support="multiple"/>
        <mesh name="CoordinateMesh"/>
        <output/>
        <stat/>
        <convergence>
          <include_in_convergence/>
        </convergence>
        <detectors>
          <include_in_detectors/>
        </detectors>
        <steady_state>
          <include_in_steady_state/>
        </steady_state>
      </diagnostic>
    </scalar_field>
    <scalar_field name="PerturbationDensity" rank="0">
      <diagnostic>
        <algorithm name="Internal" material_phase_support="multiple"/>
        <mesh name="CoordinateMesh"/>
        <output/>
        <stat>
          <exclude_from_stat/>
        </stat>
        <convergence>
          <exclude_from_convergence/>
        </convergence>
        <detectors>
          <exclude_from_detectors/>
        </detectors>
        <steady_state>
          <exclude_from_steady_state/>
        </steady_state>
      </diagnostic>
    </scalar_field>
    <scalar_field name="Temperature" rank="0">
      <prognostic>
        <mesh name="CoordinateMesh"/>
        <equation name="AdvectionDiffusion"/>
        <spatial_discretisation>
          <continuous_galerkin>
            <stabilisation>
              <streamline_upwind>
                <nu_bar_optimal/>
                <nu_scale name="0.5">
                  <real_value shape="1" rank="0">0.5</real_value>
                </nu_scale>
              </streamline_upwind>
            </stabilisation>
            <advection_terms/>
            <mass_terms/>
          </continuous_galerkin>
          <conservative_advection>
            <real_value rank="0">0.0</real_value>
          </conservative_advection>
        </spatial_discretisation>
        <temporal_discretisation>
          <theta>
            <real_value rank="0">1.0</real_value>
          </theta>
        </temporal_discretisation>
        <solver>
          <iterative_method name="gmres">
            <restart>
              <integer_value rank="0">30</integer_value>
            </restart>
          </iterative_method>
          <preconditioner name="sor"/>
          <relative_error>
            <real_value rank="0">1.0e-7</real_value>
          </relative_error>
          <max_iterations>
            <integer_value rank="0">1500</integer_value>
          </max_iterations>
          <never_ignore_solver_failures/>
          <diagnostics>
            <monitors/>
          </diagnostics>
        </solver>
        <initial_condition name="WholeMesh">
          <python>
            <string_value lines="20" type="code" language="python">def val(X,t):
  #from numpy import interp, genfromtxt
  #depth, temp = genfromtxt("preprocess/temperature.dat", unpack=True)
  #return interp (X[2], depth, temp)
  
  #if X[2] &gt; -50.0:
  #  return 8.5
  #elif X[2] &gt; -200.0:
  #  return 8.5 + 3.0 * (X[2] + 50.) / 150.
  #else:
  #  return 5.5
  
  if X[2] &gt; -1.5:
    return 8.5
  else:
    return 8.5 + 3.0 * X[2] / 308.</string_value>
          </python>
        </initial_condition>
        <boundary_conditions name="no_flux_solid">
          <surface_ids>
            <integer_value shape="5" rank="1">1001 1002 1003 1004 2002</integer_value>
          </surface_ids>
          <type name="neumann">
            <constant>
              <real_value rank="0">0.0</real_value>
            </constant>
          </type>
        </boundary_conditions>
        <boundary_conditions name="surface">
          <surface_ids>
            <integer_value shape="1" rank="1">2001</integer_value>
          </surface_ids>
          <type name="neumann">
            <constant>
              <real_value rank="0">0.0</real_value>
            </constant>
          </type>
        </boundary_conditions>
        <subgridscale_parameterisation name="GLS"/>
        <output/>
        <stat/>
        <convergence>
          <include_in_convergence/>
        </convergence>
        <detectors>
          <include_in_detectors/>
        </detectors>
        <steady_state>
          <include_in_steady_state/>
        </steady_state>
        <consistent_interpolation/>
      </prognostic>
    </scalar_field>
    <scalar_field name="GeostrophicPressure" rank="0">
      <prognostic>
        <mesh name="GeostrophicPressureMesh"/>
        <spatial_discretisation>
          <geostrophic_pressure_option>
            <string_value>include_buoyancy</string_value>
          </geostrophic_pressure_option>
        </spatial_discretisation>
        <solver>
          <iterative_method name="cg"/>
          <preconditioner name="mg">
            <vertical_lumping/>
          </preconditioner>
          <relative_error>
            <real_value rank="0">1.0e-8</real_value>
          </relative_error>
          <max_iterations>
            <integer_value rank="0">1000</integer_value>
          </max_iterations>
          <remove_null_space/>
          <never_ignore_solver_failures/>
          <diagnostics>
            <monitors/>
          </diagnostics>
        </solver>
        <output/>
        <stat>
          <exclude_from_stat/>
        </stat>
        <convergence>
          <exclude_from_convergence/>
        </convergence>
        <detectors>
          <exclude_from_detectors/>
        </detectors>
        <steady_state>
          <exclude_from_steady_state/>
        </steady_state>
        <no_interpolation/>
      </prognostic>
    </scalar_field>
    <scalar_field name="CFLNumber" rank="0">
      <diagnostic>
        <algorithm name="Internal" material_phase_support="multiple"/>
        <mesh name="CoordinateMesh"/>
        <output/>
        <stat/>
        <convergence>
          <include_in_convergence/>
        </convergence>
        <detectors>
          <include_in_detectors/>
        </detectors>
        <steady_state>
          <include_in_steady_state/>
        </steady_state>
      </diagnostic>
    </scalar_field>
  </material_phase>
</fluidity_options>
