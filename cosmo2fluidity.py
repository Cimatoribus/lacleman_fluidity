#!/usr/bin/python

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
from datetime import datetime
from matplotlib.dates import date2num, num2date
from os.path import join
import vtktools

class GPSConverter(object):
    '''
    GPS Converter class which is able to perform convertions between the 
    CH1903 and WGS84 system.
    '''
    # Convert CH y/x/h to WGS height
    def CHtoWGSheight(self, y, x, h):
        # Axiliary values (% Bern)
        y_aux = (y - 600000) / 1000000
        x_aux = (x - 200000) / 1000000
        h = (h + 49.55) - (12.60 * y_aux) - (22.64 * x_aux)
        return h

    # Convert CH y/x to WGS lat
    def CHtoWGSlat(self, y, x):
        # Axiliary values (% Bern)
        y_aux = (y - 600000) / 1000000
        x_aux = (x - 200000) / 1000000
        lat = (16.9023892 + (3.238272 * x_aux)) + \
                - (0.270978 * pow(y_aux, 2)) + \
                - (0.002528 * pow(x_aux, 2)) + \
                - (0.0447 * pow(y_aux, 2) * x_aux) + \
                - (0.0140 * pow(x_aux, 3))
        # Unit 10000" to 1" and convert seconds to degrees (dec)
        lat = (lat * 100) / 36
        return lat

    # Convert CH y/x to WGS long
    def CHtoWGSlng(self, y, x):
        # Axiliary values (% Bern)
        y_aux = (y - 600000) / 1000000
        x_aux = (x - 200000) / 1000000
        lng = (2.6779094 + (4.728982 * y_aux) + \
                + (0.791484 * y_aux * x_aux) + \
                + (0.1306 * y_aux * pow(x_aux, 2))) + \
                - (0.0436 * pow(y_aux, 3))
        # Unit 10000" to 1" and convert seconds to degrees (dec)
        lng = (lng * 100) / 36
        return lng

    # Convert decimal angle (deg dec) to sexagesimal angle (dd.mmss,ss)
    def DecToSexAngle(self, dec):
        degree = int(np.floor(dec))
        minute = int(np.floor((dec - degree) * 60))
        second = (((dec - degree) * 60) - minute) * 60
        return degree + (float(minute) / 100) + (second / 10000)
		
    # Convert sexagesimal angle (dd.mmss,ss) to seconds
    def SexAngleToSeconds(self, dms):
        degree = 0 
        minute = 0 
        second = 0
        degree = np.floor(dms)
        minute = np.floor((dms - degree) * 100)
        second = (((dms - degree) * 100) - minute) * 100
        return second + (minute * 60) + (degree * 3600)

    # Convert sexagesimal angle (dd.mmss) to decimal angle (degrees)
    def SexToDecAngle(self, dms):
        degree = 0
        minute = 0
        second = 0
        degree = np.floor(dms)
        minute = np.floor((dms - degree) * 100)
        second = (((dms - degree) * 100) - minute) * 100
        return degree + (minute / 60) + (second / 3600)
    
    # Convert WGS lat/long (deg dec) and height to CH h
    def WGStoCHh(self, lat, lng, h):
        lat = self.DecToSexAngle(lat)
        lng = self.DecToSexAngle(lng)
        lat = self.SexAngleToSeconds(lat)
        lng = self.SexAngleToSeconds(lng)
        # Axiliary values (% Bern)
        lat_aux = (lat - 169028.66) / 10000
        lng_aux = (lng - 26782.5) / 10000
        h = (h - 49.55) + (2.73 * lng_aux) + (6.94 * lat_aux)
        return h

    # Convert WGS lat/long (deg dec) to CH x
    def WGStoCHx(self, lat, lng):
        lat = self.DecToSexAngle(lat)
        lng = self.DecToSexAngle(lng)
        lat = self.SexAngleToSeconds(lat)
        lng = self.SexAngleToSeconds(lng)
        # Axiliary values (% Bern)
        lat_aux = (lat - 169028.66) / 10000
        lng_aux = (lng - 26782.5) / 10000
        x = ((200147.07 + (308807.95 * lat_aux) + \
            + (3745.25 * pow(lng_aux, 2)) + \
            + (76.63 * pow(lat_aux,2))) + \
            - (194.56 * pow(lng_aux, 2) * lat_aux)) + \
            + (119.79 * pow(lat_aux, 3))
        return x

	# Convert WGS lat/long (deg dec) to CH y
    def WGStoCHy(self, lat, lng):
        lat = self.DecToSexAngle(lat)
        lng = self.DecToSexAngle(lng)
        lat = self.SexAngleToSeconds(lat)
        lng = self.SexAngleToSeconds(lng)
        # Axiliary values (% Bern)
        lat_aux = (lat - 169028.66) / 10000
        lng_aux = (lng - 26782.5) / 10000
        y = (600072.37 + (211455.93 * lng_aux)) + \
            - (10938.51 * lng_aux * lat_aux) + \
            - (0.36 * lng_aux * pow(lat_aux, 2)) + \
            - (44.54 * pow(lng_aux, 3))
        return y

    def LV03toWGS84(self, east, north, height):
        '''
        Convert LV03 to WGS84 Return a array of double that contain lat, long,
        and height
        '''
        d = []
        d.append(self.CHtoWGSlat(east, north))
        d.append(self.CHtoWGSlng(east, north))
        d.append(self.CHtoWGSheight(east, north, height))
        return d
        
    def WGS84toLV03(self, latitude, longitude, ellHeight):
        '''
        Convert WGS84 to LV03 Return an array of double that contaign east,
        north, and height
        '''
        d = []
        d.append(self.WGStoCHy(latitude, longitude))
        d.append(self.WGStoCHx(latitude, longitude))
        d.append(self.WGStoCHh(latitude, longitude, ellHeight))
        return d


# extract fluidity grid info
def get_fluidity_grid(grid_file):
    f = vtktools.vtu(grid_file)
    xyz = f.GetLocations() 
    xy = xyz[xyz[:,2]>-1e-7]
    return xy[:,0], xy[:,1]


# extract COSMO grid info
def get_COSMO_grid(cosmo_file):
    #%Extract geographical data from the first COSMO-2 file
    #date = datevec(dateini); %Date vector
    #FileName = [datapath sprintf('%i',date(1)) '\cosmo2_epfl_lakes_' sprintf('%i%02i%02i',date(1:3)) '.nc'];
    f = netcdf.netcdf_file(cosmo_file, 'r')
    lon = f.variables["lon_1"][:]
    lat = f.variables["lat_1"][:]
    f.close()
    converter = GPSConverter()
    x = np.zeros_like(lon)
    y = np.zeros_like(lat)
    for jj in xrange(x.shape[1]):
        for ii in xrange(x.shape[0]):
            x[ii,jj], y[ii,jj], _ = converter.WGS84toLV03(
                                    lat[ii, jj], lon[ii,jj], 0.0)
    return x, y

def cosmo2fluidity(fluid_file, data_path, data_root, start, end,
                   ref_date="01-01-2012 00:00:00"):
    # Reference date
    fluidity_ref = np.floor(date2num(datetime.strptime(ref_date,
                                                       '%d-%m-%Y %H:%M:%S')))
    # Fluidity grid
    xf, yf = get_fluidity_grid(fluid_file)

    # Time range to be extracted
    date_start = date2num(datetime.strptime(start, '%d-%m-%Y'))
    date_end = date2num(datetime.strptime(end, '%d-%m-%Y'))

    # Get time convention from first COSMO file
    # fname = data_root + num2date(date_start).strftime("%Y%m%d") + ".nc"
    # fname = join(data_path,fname)
    # in_f = netcdf.netcdf_file(fname, 'r')
    # cosmo_ref = in_f.variables["time"].units
    # cosmo_ref = cosmo_ref.lstrip("seconds since ")
    # cosmo_ref = date2num(datetime.strptime(cosmo_ref, "%Y-%m-%d %H:%M:%S"))
    # in_f.close()

    # Get grid info
    fname = data_root + num2date(date_start).strftime("%Y%m%d") + ".nc"
    fname = join(data_path,fname)
    xc, yc = get_COSMO_grid(fname)
    # define the limits of the output domain
    minx = xf.min() - 5e3
    maxx = xf.max() + 5e3
    miny = yf.min() - 5e3
    maxy = yf.max() + 5e3

    # limits of the input data
    minxin = xf.min() - 1e4
    maxxin = xf.max() + 1e4
    minyin = yf.min() - 1e4
    maxyin = yf.max() + 1e4
    ind = np.where((xc>=minxin) & (xc<=maxxin) & (yc>=minyin) & (yc<=maxyin))

    xc = xc[ind[0], ind[1]]
    yc = yc[ind[0], ind[1]]

    xout = np.arange(minx, maxx, 2e3)
    yout = np.arange(miny, maxy, 2e3)

    out_file = "COSMO2fluidity_wind_{}_{}.nc".format(start, end)

    out_f = netcdf.netcdf_file(out_file, 'w')
    out_f.history = "Forcing from COSMO model interpolated on fluidity grid"
    out_f.createDimension('longitude', xout.size)
    out_lon = out_f.createVariable('longitude', 'f', ('longitude',))
    out_lon.units = "metres CH1903 projection"
    out_lon[:] = xout
    out_f.createDimension('latitude', yout.size)
    out_lat = out_f.createVariable('latitude', 'f', ('latitude',))
    out_lat.units = "metres CH1903 projection"
    out_lat[:] = yout
    out_f.createDimension('time', 24*int(np.floor(date_end-date_start + 1)))

    # Generate time axis
    time = np.arange(0.0, date_end - date_start + 1., 1.0/24.0)
    time += date_start - fluidity_ref
    time *= 86400.
    out_time = out_f.createVariable("time", 'f', ("time",))
    out_time[:] = time
    out_time.units = "seconds since " + ref_date

    # generate variables to be saved
    out_U10 = out_f.createVariable("U10", 'f', ('time', 'latitude', "longitude"))
    out_U10._FillValue = 0.0
    out_V10 = out_f.createVariable("V10", 'f', ('time', "latitude", "longitude"))
    out_V10._FillValue = 0.0

    xout, yout = np.meshgrid(xout, yout)
    xc = xc.ravel()
    yc = yc.ravel()

    today = date_start
    hour = 0
    while today<=date_end:
        print("Exporting day %d" % today)
        fname = data_root + num2date(today).strftime("%Y%m%d") + ".nc"
        in_f = netcdf.netcdf_file(join(data_path,fname), 'r')

        U10 = in_f.variables["U"][:, 0, ind[0], ind[1]]
        V10 = in_f.variables["V"][:, 0, ind[0], ind[1]]

        in_f.close()

        last = U10.shape[0]
        if last<24:
            for hh in xrange(24-last):
                U10 = np.hstack(U10, U10[last, ...])
                V10 = np.hstack(V10, V10[last, ...])

        for hh in xrange(24):
            out_U10[hour,...] = griddata((xc,yc),
                                         U10[hh, ...].ravel(), (xout, yout))
            out_V10[hour,...] = griddata((xc,yc),
                                         V10[hh, ...].ravel(), (xout, yout))
            hour += 1
        
        today += 1.
    # Loop over input files
    out_f.close()


__doc__ = \
"""cosmo2fluidity.py

Script to convert surface forcing data from COSMO2 model output to fluidity grid

Usage:
    cosmo2fluidity.py <vtu> <dir> <root> <start> <end> [-h]
                   [--ref-date=<str>]

Arguments:
    <vtu>            fluidity vtu/pvtu output file. The fluidity grid is
                        obtained from this file. 
    <dir>            Directory where the COSMO2 files are located.
    <root>           Root of the file names of COSMO2 output.
    <start>          Start date, in the format "01-12-2015".
    <end>            End date, in the format "01-12-2015".
Options:
    --ref-date=<str>    Reference date used in fluidity, in the format
                        "01-12-2015 13:00:00". [default: 01-01-2012 00:00:00]

"""


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)  # applause

    cosmo2fluidity(args["<vtu>"],
                   args["<dir>"],
                   args["<root>"],
                   args["<start>"],
                   args["<end>"],
                   ref_date=args["--ref-date"])
