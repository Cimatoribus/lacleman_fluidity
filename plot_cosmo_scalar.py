#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as ppl
from scipy.io import netcdf
from scipy.interpolate import griddata
from datetime import datetime
from matplotlib.dates import date2num, num2date
from os.path import join
import vtktools

class GPSConverter(object):
    '''
    GPS Converter class which is able to perform convertions between the 
    CH1903 and WGS84 system.
    '''
    # Convert CH y/x/h to WGS height
    def CHtoWGSheight(self, y, x, h):
        # Axiliary values (% Bern)
        y_aux = (y - 600000) / 1000000
        x_aux = (x - 200000) / 1000000
        h = (h + 49.55) - (12.60 * y_aux) - (22.64 * x_aux)
        return h

    # Convert CH y/x to WGS lat
    def CHtoWGSlat(self, y, x):
        # Axiliary values (% Bern)
        y_aux = (y - 600000) / 1000000
        x_aux = (x - 200000) / 1000000
        lat = (16.9023892 + (3.238272 * x_aux)) + \
                - (0.270978 * pow(y_aux, 2)) + \
                - (0.002528 * pow(x_aux, 2)) + \
                - (0.0447 * pow(y_aux, 2) * x_aux) + \
                - (0.0140 * pow(x_aux, 3))
        # Unit 10000" to 1" and convert seconds to degrees (dec)
        lat = (lat * 100) / 36
        return lat

    # Convert CH y/x to WGS long
    def CHtoWGSlng(self, y, x):
        # Axiliary values (% Bern)
        y_aux = (y - 600000) / 1000000
        x_aux = (x - 200000) / 1000000
        lng = (2.6779094 + (4.728982 * y_aux) + \
                + (0.791484 * y_aux * x_aux) + \
                + (0.1306 * y_aux * pow(x_aux, 2))) + \
                - (0.0436 * pow(y_aux, 3))
        # Unit 10000" to 1" and convert seconds to degrees (dec)
        lng = (lng * 100) / 36
        return lng

    # Convert decimal angle (deg dec) to sexagesimal angle (dd.mmss,ss)
    def DecToSexAngle(self, dec):
        degree = int(np.floor(dec))
        minute = int(np.floor((dec - degree) * 60))
        second = (((dec - degree) * 60) - minute) * 60
        return degree + (float(minute) / 100) + (second / 10000)
		
    # Convert sexagesimal angle (dd.mmss,ss) to seconds
    def SexAngleToSeconds(self, dms):
        degree = 0 
        minute = 0 
        second = 0
        degree = np.floor(dms)
        minute = np.floor((dms - degree) * 100)
        second = (((dms - degree) * 100) - minute) * 100
        return second + (minute * 60) + (degree * 3600)

    # Convert sexagesimal angle (dd.mmss) to decimal angle (degrees)
    def SexToDecAngle(self, dms):
        degree = 0
        minute = 0
        second = 0
        degree = np.floor(dms)
        minute = np.floor((dms - degree) * 100)
        second = (((dms - degree) * 100) - minute) * 100
        return degree + (minute / 60) + (second / 3600)
    
    # Convert WGS lat/long (deg dec) and height to CH h
    def WGStoCHh(self, lat, lng, h):
        lat = self.DecToSexAngle(lat)
        lng = self.DecToSexAngle(lng)
        lat = self.SexAngleToSeconds(lat)
        lng = self.SexAngleToSeconds(lng)
        # Axiliary values (% Bern)
        lat_aux = (lat - 169028.66) / 10000
        lng_aux = (lng - 26782.5) / 10000
        h = (h - 49.55) + (2.73 * lng_aux) + (6.94 * lat_aux)
        return h

    # Convert WGS lat/long (deg dec) to CH x
    def WGStoCHx(self, lat, lng):
        lat = self.DecToSexAngle(lat)
        lng = self.DecToSexAngle(lng)
        lat = self.SexAngleToSeconds(lat)
        lng = self.SexAngleToSeconds(lng)
        # Axiliary values (% Bern)
        lat_aux = (lat - 169028.66) / 10000
        lng_aux = (lng - 26782.5) / 10000
        x = ((200147.07 + (308807.95 * lat_aux) + \
            + (3745.25 * pow(lng_aux, 2)) + \
            + (76.63 * pow(lat_aux,2))) + \
            - (194.56 * pow(lng_aux, 2) * lat_aux)) + \
            + (119.79 * pow(lat_aux, 3))
        return x

	# Convert WGS lat/long (deg dec) to CH y
    def WGStoCHy(self, lat, lng):
        lat = self.DecToSexAngle(lat)
        lng = self.DecToSexAngle(lng)
        lat = self.SexAngleToSeconds(lat)
        lng = self.SexAngleToSeconds(lng)
        # Axiliary values (% Bern)
        lat_aux = (lat - 169028.66) / 10000
        lng_aux = (lng - 26782.5) / 10000
        y = (600072.37 + (211455.93 * lng_aux)) + \
            - (10938.51 * lng_aux * lat_aux) + \
            - (0.36 * lng_aux * pow(lat_aux, 2)) + \
            - (44.54 * pow(lng_aux, 3))
        return y

    def LV03toWGS84(self, east, north, height):
        '''
        Convert LV03 to WGS84 Return a array of double that contain lat, long,
        and height
        '''
        d = []
        d.append(self.CHtoWGSlat(east, north))
        d.append(self.CHtoWGSlng(east, north))
        d.append(self.CHtoWGSheight(east, north, height))
        return d
        
    def WGS84toLV03(self, latitude, longitude, ellHeight):
        '''
        Convert WGS84 to LV03 Return an array of double that contaign east,
        north, and height
        '''
        d = []
        d.append(self.WGStoCHy(latitude, longitude))
        d.append(self.WGStoCHx(latitude, longitude))
        d.append(self.WGStoCHh(latitude, longitude, ellHeight))
        return d


# extract COSMO grid info
def get_COSMO_grid(cosmo_file):
    #%Extract geographical data from the first COSMO-2 file
    #date = datevec(dateini); %Date vector
    #FileName = [datapath sprintf('%i',date(1)) '\cosmo2_epfl_lakes_' sprintf('%i%02i%02i',date(1:3)) '.nc'];
    f = netcdf.netcdf_file(cosmo_file, 'r')
    lon = f.variables["lon_1"][:]
    lat = f.variables["lat_1"][:]
    f.close()
    converter = GPSConverter()
    x = np.zeros_like(lon)
    y = np.zeros_like(lat)
    for jj in xrange(x.shape[1]):
        for ii in xrange(x.shape[0]):
            x[ii,jj], y[ii,jj], _ = converter.WGS84toLV03(
                                    lat[ii, jj], lon[ii,jj], 0.0)
    return x, y


def plot_cosmo_scalar(input, data_path, data_root, date, cosmovar, era40var):


    # Time range to be extracted
    date = date2num(datetime.strptime(date, '%Y-%m-%d %H:%M'))

    in_f = netcdf.netcdf_file(input, 'r')
    ref_date = in_f.variables["time"].units
    ref_date = ref_date.lstrip("seconds since ")
    ref_date = ref_date.rstrip("0").rstrip(".")
    ref_date = date2num(datetime.strptime(ref_date, '%Y-%m-%d %H:%M:%S'))

    xi = in_f.variables["longitude"][:].copy()
    yi = in_f.variables["latitude"][:].copy()

    time = int( 24 * (date - ref_date))

    dtype = in_f.variables[era40var][:].dtype
    var_f = np.float64(np.copy(in_f.variables[era40var][time, ...]))
    if np.issubdtype(dtype, np.int):
        try:
            offset = in_f.variables[era40var].add_offset
            scale = in_f.variables[era40var].scale_factor
            var_f *= scale
            var_f += offset
        except AttributeError:
            print("Fluidity file contains integers, but I could "
                  "not find a scaling factor and offset.")

    in_f.close()

    # Get grid info
    fname = data_root + num2date(int(date)).strftime("%Y%m%d") + ".nc"
    fname = join(data_path,fname)
    xc, yc = get_COSMO_grid(fname)

    # define the limits of the plot domain
    minx = xi.min() - 1e4
    maxx = xi.max() + 1e4
    miny = yi.min() - 1e4
    maxy = yi.max() + 1e4

    ind = np.where((xc>=minx) & (xc<=maxx) & (yc>=miny) & (yc<=maxy))

    xc = xc[ind[0], ind[1]]
    yc = yc[ind[0], ind[1]]

    fname = data_root + num2date(int(date)).strftime("%Y%m%d") + ".nc"
    in_f = netcdf.netcdf_file(join(data_path,fname), 'r')

    hour = int((date % 1) * 24)

    cvar = in_f.variables[cosmovar]
    ndim = len(cvar.shape)
    if ndim == 3:
        var_c = cvar[hour, ind[0], ind[1]]
    elif ndim == 4:
        # For multi-layer data, we only look at 10m
        var_c = cvar[hour, 0, ind[0], ind[1]]

    in_f.close()

    xp = np.linspace(xi.min(), xi.max(), 100)
    yp = np.linspace(yi.min(), yi.max(), 100)
    xplot, yplot = np.meshgrid(xp, yp)
    out_var = griddata((xc,yc), var_c.ravel(), (xplot, yplot))

    # Convert to km
    xi *= 1e-3
    yi *= 1e-3
    xp *= 1e-3
    yp *= 1e-3

    # Load shoreline
    xs, ys = np.genfromtxt("/media/space/Data/Bathy_Lac_Leman/shoreline.ldb",
                           unpack=True)
    xs *= 1.e-3
    ys *= 1.e-3

    f = ppl.figure()
    ax = f.add_subplot(111)
    
    minval = np.percentile(out_var, 2)
    maxval = np.percentile(out_var, 98)

    if minval == maxval:
        if np.all(out_var==0.) & np.all(var_f==0.):
            print("Both fields are zero.")
            return

    C = ax.contourf(xp, yp, out_var, 40, vmin=minval, vmax=maxval, alpha=0.7)
    ppl.colorbar(C)

    ax.contour(xi, yi, var_f, 40, vmin=minval, vmax=maxval, lw=2)

    # shore
    ax.plot(xs, ys, 'k-', lw=0.8)

    ax.set_xlabel("Lat (km CH1903)")
    ax.set_ylabel("Lon (km CH1903)")

    f.suptitle("Cosmo (fill): {}, fluidity (contours): {}".format(cosmovar, era40var))

    ax.grid("off")

    ppl.show()

__doc__ = \
"""plot_cosmo_scalar.py

Plot COSMO scalar input and interpolated version for fluidity (usually,
ERA40-style).

Usage:
    plot_cosmo_scalar.py <fld> <dir> <root> <date> -c <COSMOVAR> -e <ERA40VAR> [-h]

Arguments:
    <fld>            NetCDF file for fluidity BC computed by cosmo2fluidity.py
    <dir>            Directory where the COSMO2 files are located.
    <root>           Root of the file names of COSMO2 output.
    <date>           Date to plot, in the format "01-12-2015 23:00".

Options:
    -c <COSMOVAR>    Variable name in the COSMO2 file.
    -e <ERA40VAR>    (Corresponding) variable name in ERA40 file.

"""


if __name__ == '__main__':
    from docopt import docopt
    args = docopt(__doc__)  # applause

    plot_cosmo_scalar(args["<fld>"],
                      args["<dir>"],
                      args["<root>"],
                      args["<date>"],
                      args["-c"],
                      args["-e"])
