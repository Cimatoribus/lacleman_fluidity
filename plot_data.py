import vtk
import glob
import sys
import os
import matplotlib.pyplot as ppl
import numpy as np
from fluidity_tools import stat_parser
import le_tools


################################################
#----------------- PROGRESS--------------------# 
################################################

def progress(flmlname):

  model_time = []
  wall_time = []
  CFL = []
  v_max = []
  rho_min = []
  rho_max = []
  
  # get stat files 
  # time_index_end used to ensure don't repeat values
  stat_files, time_index_end = le_tools.GetstatFiles('./')
  for i in range(len(stat_files)):
    stat = stat_parser(stat_files[i])
    for j in range(time_index_end[i]):
      model_time.append(stat['ElapsedTime']['value'][j])
      wall_time.append(stat['ElapsedWallTime']['value'][j])
# DG      CFL.append(stat['Fields']['DG_CourantNumber']['max'][j])
      CFL.append(stat['Fields']['CFLNumber']['max'][j])
      v_max.append(stat['Fields']['Velocity%magnitude']['max'][j])
      rho_max.append(stat['Fields']['Temperature']['max'][j])
      rho_min.append(stat['Fields']['Temperature']['min'][j])
      
  # plot
  f = ppl.figure(figsize = (12, 9))
  
  ax = f.add_subplot(221)
  ax.plot(model_time, wall_time, 'b.', ms = 10)
  
  ax.set_xlim(min(model_time), max(model_time))
  ax.set_xlabel('Model time (s)')
  ax.set_ylabel('Wall clock time (s)')
  
  ax = f.add_subplot(222)
  ax.plot(model_time, CFL, 'b.', ms = 10)
  
  ax.set_xlim(min(model_time), max(model_time))
  ax.set_xlabel('Model time (s)')
  ax.set_ylabel('CFL number')
  
  ax = f.add_subplot(223)
  ax.plot(model_time, v_max, 'b.', ms = 10)
  
  ax.set_xlim(min(model_time), max(model_time))
  ax.set_xlabel('Model time (s)')
  ax.set_ylabel('Max velocity (m/s)')

  ax = f.add_subplot(224)
  # ax.plot(model_time, rho_min, 'b.', ms = 10)
  ax.plot(model_time, rho_max, 'r.', ms = 10)
  
  ax.set_xlim(min(model_time), max(model_time))
  ax.set_xlabel('Model time (s)')
  ax.set_ylabel('Max temp (deg C)')

  ppl.tight_layout()

  f.savefig('diagnostics/plots/progress.png')
  return

################################################
#---------------- MAIN CALLS ------------------# 
################################################

# make directories to put diagnostics in (if they don't exist already)
for name in ['diagnostics', 'diagnostics/logs', 'diagnostics/plots']:
  try: os.stat(name)
  except OSError: os.mkdir(name)
    
# set flmlname
flmlname = './lacleman.flml'

# plot basic progress of the simulation
progress(flmlname)  

# show plots and tell user where to find a copy
ppl.show()
print 'The images have also been saved in ./diagnostics/plots'
